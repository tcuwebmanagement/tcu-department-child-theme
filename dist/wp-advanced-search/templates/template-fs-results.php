<?php
/*
Falculty & Staff Results Template

This is a template part which can be used to customize how search
results appear when using AJAX.
*/
?>

<?php if (have_posts()): ?>
   <?php while (have_posts()): the_post(); ?>

        <!-- <h2><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h2> -->
        
        <?//php the_field('last_name');?>
        <!-- <p><a href="<?php the_permalink(); ?>">Read more...</a></p> -->
        <section class="falculty-staff-search">
              <h2>Faculty &amp; Staff Results</h2>
              <!-- <div id="wpas-results"></div> -->
            <table class="tcu-table tcu-table--border">
              <tbody>
                <tr id="post-<?php the_ID(); ?>">
                  <td id="wpas-results"></td>
                </tr>
              </tbody>
            </table>
        

    <?php endwhile; ?>

<?php else : ?>

    <p>Sorry, no posts matched your criteria.</p>

<?php endif; ?>

<?php wp_reset_query(); ?>