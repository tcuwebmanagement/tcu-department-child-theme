<?php
/*
 * Template Name: Tabs (with right sidebar)
 */

?>
<?php get_header(); ?>

    <?php if (function_exists('tcu_breadcrumbs_list')) {
    tcu_breadcrumbs_list();
} ?>

<div class="tcu-layoutwrap--transparent">

    <div class="tcu-layout-constrain cf">

        <?php
        /*
         * We add #main to our content element because we have a skip
         * main navigation link for accessibility
         */
        ?>
        <main class="unit size2of3 m-size2of3 tcu-below32 cf" id="main">

        <?php if (have_posts()):

        /*
         * Start the loop.
         */
        while (have_posts()) : the_post(); ?>

        <h2><?php the_title(); ?></h2>

        <article id="post-<?php the_ID(); ?>" <?php post_class('tcu-article cf'); ?>>

         <?php if (get_field('tabs_repeater')):  ?>

            <!-- Responvise Tabs  -->
            <div class="tcu-responsive-tabs">

                <!-- Begin UL -->
                <ul>
                    <?php
                    /*
                     * Start the ACF loop.
                     */
                    while (have_rows('tabs_repeater')): the_row();

                    /*
                     * ACF title variable
                     */
                    $tabs_title = get_sub_field('tab_title');
                    $title = preg_replace('![^a-z0-9]+!i', '_', $tabs_title);

                    ?>

                    <li><a href="#<?php echo strtolower($title); ?>"><?php the_sub_field('tab_title'); ?></a></li>

                    <?php
                    /*
                     * End the ACF loop.
                     */
                    endwhile;
                    ?>
                </ul><!-- End UL -->

                <?php
                /*
                 * Start the ACF loop.
                 */
                while (have_rows('tabs_repeater')): the_row();

                /*
                 * ACF title variable
                 */
                $tabs_title = get_sub_field('tab_title');
                $title = preg_replace('![^a-z0-9]+!i', '_', $tabs_title);
                ?>

                <div class="tcu-article__content cf" role="region" id="<?php echo strtolower($title); ?>"><?php the_sub_field('tab_content'); ?>
                </div>

                <?php
                /*
                 * End of the ACF loop.
                 */
                endwhile; ?>
            </div>

        <?php endif; ?>

        </article><!-- end of .tcu-article -->

        <?php

        /*
         * End of the WP loop
         */
        endwhile;

            else:

                // If no content, include the "No posts found" template.
                template_part('partials/content', 'none');

        endif;
        ?>

        </main><!-- end of .unit -->

        <?php get_sidebar(); ?>

    </div><!-- end of .tcu-layout-constrain -->

</div><!-- end .tcu-layoutwrap--transparent -->

<?php get_footer(); ?>
