<?php
/**
 * The template for displaying a message that posts cannot be found
 */
?>
<div class="tcu-layoutwrap--transparent tcu-pad-lr0 cf">
    <div class="tcu-layout-constrain tcu-infographics cf">
        <article class="tcu-article hentry cf" role="article">
            <header class="tcu-article__header">
                <h2><?php _e('Oops, Post Not Found!', 'tcu-academic-child-theme'); ?></h2>
            </header>
            <section class="tcu-article__content" role="region">
                <p><?php _e('Uh Oh. Something is missing. Try double checking things.', 'tcu-academic-child-theme'); ?></p>
            </section>
        </article>
    </div>
</div>