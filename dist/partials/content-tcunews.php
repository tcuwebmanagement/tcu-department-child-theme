<?php
/**
 * Template part to display a news feed
 */

// ACF variables
$title     = get_sub_field('tcu_news_title');
$date      = get_sub_field('tcu_news_display_date');
$images    = get_sub_field('tcu_news_display_images');
$excerpt   = get_sub_field('tcu_news_display_excerpt');
$keywords  = get_sub_field('tcu_news_display_keywords');
$num_posts = get_sub_field('tcu_news_number_of_posts_to_show');
$terms     = get_sub_field('tcu_news_select_keyword');
$link      = get_sub_field('tcu_news_link');

// Extract slug within parantheses
if (preg_match('#\((.*?)\)#', $terms, $match)) {
    $slug = $match[1];
} else {
    $slug = '';
}

// Set up our query
if (trim($slug) === 'tcu_all_news') {
    if (function_exists('tcu_news_query')) {
        $posts =  tcu_news_query($num_posts);
    }
} else {
    if (function_exists('tcu_news_query')) {
        $posts = tcu_news_query($num_posts, $slug);
    }
}

if (count($posts)): ?>
<div class="tcu-layoutwrap--transparent cf">

    <div class="tcu-layout-constrain cf">

    <?php if ($title) : ?>
        <h4 class="tcu-mar-t0 tcu-mar-t0 tcu-arvo tcu-alignc h2"><?php echo $title; ?></h4>
        <?php endif; ?>

        <div class="tcu-masonry-home effect-2 cf" id="tcu-masonry-home">

            <?php foreach ($posts as $post): setup_postdata($post); ?>

            <article class="tcu-article tcu-modal cf" role="article">

                <?php if ($images) {
                    if (has_post_thumbnail()) {
                        the_post_thumbnail('tcu_news_thumb');
                    }
                } ?>

                <section class="tcu-modal__content">

                    <h5 class="tcu-arvo tcu-mar-b0 h4"><?php the_title(); ?></h5>

                    <?php if ($date) : ?>
                    <p class="tcu-byline">
                    	<span><time class="updated entry-time" datetime="<?php echo get_the_time('Y-m-d'); ?>" itemprop="datePublished"><?php echo get_the_time(get_option('date_format')); ?></time></span>
                    </p>
                    <?php endif; ?>

                    <?php
                    if ($excerpt) :
                        $excerpt = substr(get_the_excerpt(), 0, 150);
                        echo $excerpt . '...';
                    endif;
                    ?>

                    <?php if ($keywords) : ?>
                    <p><?php echo get_the_term_list($post->ID, 'tcu_news_keyword', '', ' / ', ''); ?></p>
                    <?php endif; ?>

                </section><!-- end of tcu-modal__content -->

                <!-- Our Read More button -->
                <a title="<?php the_title_attribute(); ?>" class="tcu-button tcu-button--primary tcu-bounce tcu-bounce--right--grey tcu-full-width" href="<?php the_permalink() ?>">Read More<svg height="30" width="30"><use xlink:href="#play-icon"></use></svg></a>

            </article><!-- end of .tcu-article -->

            <?php
            endforeach;
            wp_reset_postdata();
            ?>

        </div><!-- end of .tcu-masonry -->
        <?php endif; ?>

        <div class="tcu-layout-center tcu-alignc tcu-top32 tcu-below32">
            <!-- Our Read More button -->
            <a title="Read More News" class="tcu-button tcu-button--primary tcu-bounce tcu-bounce--right--grey tcu-alignc" href="<?php echo esc_url($link); ?>">More News</a>
        </div>

    </div><!-- end of .tcu-layout-constrain -->

</div><!-- end of .tcu-layoutwrap--transparent -->
