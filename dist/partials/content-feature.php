<?php
/*
 * Template part to display feature section
 */

// ACF variables
$content   = get_sub_field('content_feature_section');
$link      = get_sub_field('link_feature_section');
$text      = get_sub_field('link_text_feature_section');
$arialabel = get_sub_field('aria_label_feature_section');
?>
<div class="tcu-layoutwrap--purple tcu-background--sayagata">

    <div class="tcu-layout--large tcu-flexbox tcu-flexbox--column tcu-flexbox--align-items tcu-flexbox--vertical-align tcu-layout-center">

      <div style="max-width: 850px;" class="tcu-layout-constrain tcu-layout-center tcu-article__content">

        <?php if (! empty($content)) :
                echo $content;
         endif;

        $html  = '<p class="tcu-alignc"><a';
        if($arialabel) {
            // Add a space at the front so it concatenates correctly
            $html .= ' aria-label="'.esc_attr($arialabel).'"';
        }
        // Add a space at the front so it concatenates correctly
        $html .= ' class="tcu-button tcu-button--transparent tcu-bounce tcu-bounce--right--yellow tcu-top16"';
        $html .= ' href="'.esc_url($link).'">'.sanitize_text_field($text).' <svg height="30" width="30" class="tcu-button-icon"><use xlink:href="#circle-next-arrow"></use></svg></a></p>';

        // Display our correct hyperlink tag
        echo $html;
        ?>
      </div>

    </div>

</div><!-- end of .tcu-layoutwrap-purple -->
