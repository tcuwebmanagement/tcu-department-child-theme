<?php
/*
 * Template part to display Quote Banner
 */
?>
<div class="tcu-layoutwrap--transparent tcu-alignc tcu-pattern--angled-lines-small">
    <div class="tcu-layout-constrain tcu-banner">
        <span class="tcu-moonface tcu-alignc tcu-line-height-1"><?php the_sub_field('quote_quote_section'); ?></span>
    </div>
</div>

