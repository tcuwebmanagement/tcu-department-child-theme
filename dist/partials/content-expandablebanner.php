<?php
/*
 * Template part to display expandable banner
 */

// ACF Variables
$image = get_sub_field('expandable_banner_image');
/**
 * We inlined our styles in order to change the image size for each Media Query
 * Faster loading time and smaller images for mobile
 * Performance is important!
 */

if (! empty($image)) : ?>
<style>
  .tcu-expandablebanner-background {
      background: url('<?php echo esc_url($image["sizes"]["tcu-480-550"]); ?>') center center no-repeat;
      background-size: cover;
      margin-left: auto;
      margin-right: auto;
    }

    @media screen and ( min-width: 481px ) {
      .tcu-expandablebanner-background {
        background: url('<?php echo esc_url($image["sizes"]["tcu-1000-550"]); ?>') center center no-repeat;
        background-size: cover;
      }
    }

    @media screen and ( min-width: 1000px ) {
    .tcu-expandablebanner-background {
        background: url('<?php echo esc_url($image["sizes"]["tcu-1800-550"]); ?>') center center no-repeat;
        background-size: cover;
      }
    }

    @media screen and ( min-width: 1200px ) {
    .tcu-expandablebanner-background {
        background: url('<?php echo esc_url($image["url"]); ?>') center center no-repeat;
        background-size: cover;
      }
    }
</style>
<?php endif; ?>
<div class="tcu-layoutwrap--aqua tcu-alignc">

    <h3 class="tcu-uppercase h2"><?php the_sub_field('expandable_banner_title'); ?></h3>

    <button type="button" class="tcu-arrow-down tcu-pulse tcu-layout-center"><span class="tcu-visuallyhidden">Expand</span></button>

    <div class="tcu-layout--large tcu-flexbox tcu-flexbox--vertical-align tcu-expandablebanner-background cf">

        <?php if (have_rows('expandable_banner_repeatable_blocks')):

            /*
             * Start the ACF loop.
             */
            while (have_rows('expandable_banner_repeatable_blocks')): the_row(); ?>

            <div class="tcu-overlay tcu-flexbox tcu-flexbox--column tcu-flexbox--vertical-align tcu-expand-banner-child">
                <a class="h4" href="<?php the_sub_field('expandable_banner_link'); ?>"><?php the_sub_field('expandable_banner_title'); ?></a>
            </div>
        <?php

        /*
         * End of the ACF loop.
         */
        endwhile;

    endif;
    ?>
    </div><!-- end of .tcu-departments -->

  </div><!-- end of .tcu-layoutwrap--aqua -->