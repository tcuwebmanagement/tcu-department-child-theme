<?php
/**
 * Template part to display our slider
 */

// ACF variables
$select = get_sub_field('select_slider');

// find the Slider ID
$id = preg_replace('/[^0-9]/', "", $select);

?>
<div class="tcu-layoutwrap--grey tcu-pad-tb0 tcu-pad-lr0 cf">
    <div class="tcu-layout--large tcu-layout-center">
        <?php echo do_shortcode('[tcu_slider id="'. $id .'"]'); ?>
    </div>
</div>