<?php
/*
 * Template part to display the hero image section
 */

// ACF Variables
$image = get_sub_field('image_image_section');
$text  = get_sub_field('content_image_section');

/**
 * We inlined our styles in order to change the image size for each Media Query
 * Faster loading time and smaller images for mobile
 * Performance is important!
 */

if (! empty($image)) : ?>
<style>
    .tcu-hero {
        background: #111111 url('<?php echo esc_url($image["sizes"]["tcu-480-550"]); ?>') center center no-repeat;
        background-size: cover;
    }

    @media screen and ( min-width: 481px ) {
        .tcu-hero {
            background: #111111 url('<?php echo esc_url($image["sizes"]["tcu-1000-550"]); ?>') center center no-repeat;
            background-size: cover;
        }
    }

    @media screen and ( min-width: 1000px ) {
    .tcu-hero {
            background: #111111 url('<?php echo esc_url($image["sizes"]["tcu-1800-550"]); ?>') center center no-repeat;
            background-size: cover;
        }
    }

    @media screen and ( min-width: 1200px ) {
        .tcu-hero {
            background: #111111 url('<?php echo esc_url($image["url"]); ?>') center center no-repeat;
            background-size: cover;
        }
    }
</style>
<?php endif; ?>

<div class="tcu-layoutwrap--purple tcu-pad-tb0 tcu-pad-lr0">
    <div class="tcu-hero tcu-flexbox tcu-flexbox--column tcu-flexbox--vertical-align">
        <?php if (! empty($text)) : ?>
        <div class="tcu-hero__content">
            <h2 class="tcu-line-height-1 tcu-arvo"><?php echo sanitize_text_field($text); ?></h2>
        </div>
        <?php endif; ?>
    </div>
</div>
