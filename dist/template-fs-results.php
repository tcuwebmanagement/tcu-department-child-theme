<?php
/*
 * Falculty & Staff Results Template
 * Advance WP Search Form
 */
?>
<style>
@media screen and (min-width: 62.5em) {
    .tcu-table--fs table {
        display: table;
        width: 100%;
        border-collapse: collapse;
    }
}

.tcu-table--fs tbody td {
    display: block;
    float: none;
    width: 100%;
}

@media screen and (min-width: 30.0625em) {
    .tcu-table--fs tbody td {
        display: block;
        float: left;
        padding-bottom: 0;
        width: 50%;
    }
}

@media screen and (min-width: 62.5em) {
    .tcu-table--fs tbody td {
        display: table-cell;
        padding-bottom: 15px;
        max-width: 20%;
    }
}

@media screen and (min-width: 62.5em) {
    .tcu-table--fs tbody tr {
        padding-bottom: 15px;
    }
}
</style>
<table class="tcu-table tcu-table--fs">

    <thead><h3><?php _e('Your search Results'); ?></h3></thead>

    <?php if (have_posts()):

    /*
     * Start the loop.
     */
    while (have_posts()): the_post();  ?>

    <tbody>
        <tr id="post-<?php the_ID(); ?>">

            <?php
            /*
             * Grab the department and array key
             */
            $current_dept = wp_get_post_terms(get_the_ID(), 'department', array('fields' => 'names', 'parent' => 0));
            $key = key($current_dept);
            ?>

            <td><a href="<?php the_permalink() ?>" rel="bookmark" title="<?php the_title_attribute(); ?>"><?php the_title(); ?></a></td>
            <td><?php the_field('tcu_title'); ?></td>
            <td><?php the_field('tcu_phone_number'); ?></td>
            <td><a href="mailto:<?php the_field('tcu_email'); ?>"><?php the_field('tcu_email'); ?></a></td>
            <?php if ($current_dept) : ?>
            <td><?php echo $current_dept[$key]; ?></td>
            <?php endif; ?>

        </tr><!-- end of tr -->

    <?php
    /*
    * End of the loop.
    */
    endwhile;
    wp_reset_query();

    else :

        _e('<p>Sorry, there are no names that matched your search criteria.</p>');

    endif;
    ?>

    </tbody>

</table><!-- end of .tcu-table -->