<?php
/**
 *  Template Name: Flexible Content
 */

get_header();

if (function_exists('tcu_breadcrumbs_list')) {
         tcu_breadcrumbs_list();
    } ?>

        <?php
        /*
         * We add #main to our content element because we have a skip
         * main navigation link for accessibility
         */
        ?>
        <main id="main">

            <?php // check if the flexible content field has rows of data
            if (have_rows('flexible_content')):

                // loop through the rows of data
                while (have_rows('flexible_content')) : the_row();

                    // check if current row layout
                    if (get_row_layout() == 'image_section') :

                        get_template_part('partials/content', 'heroimage');

                    elseif (get_row_layout() == 'video_section') :

                        get_template_part('partials/content', 'video');

                    elseif (get_row_layout() == 'overview_section') :

                        get_template_part('partials/content', 'overview');

                    elseif (get_row_layout() == 'feature_section') :

                        get_template_part('partials/content', 'feature');

                    elseif (get_row_layout() == 'highlight_section') :

                        get_template_part('partials/content', 'highlightbox');

                    elseif (get_row_layout() == 'infographics_section') :

                        get_template_part('partials/content', 'infographics');

                    elseif (get_row_layout() == 'quote_section') :

                        get_template_part('partials/content', 'banner');

                    elseif (get_row_layout() == 'expandable_banner') :

                        get_template_part('partials/content', 'expandablebanner');

                    elseif (get_row_layout() == 'slider_section') :

                        get_template_part('partials/content', 'slider');

                    elseif (get_row_layout() == 'tcu_news_section') :

                        get_template_part('partials/content', 'tcunews');

                    elseif (get_row_layout() == 'rss_tcu_news_feed') :

                        get_template_part('partials/content', 'rss-news');

                    endif;

             endwhile;  ?>

        </main><!-- end of #main -->

        <?php else :

            get_template_part('partials/content', 'none');

        endif;

get_footer(); ?>
