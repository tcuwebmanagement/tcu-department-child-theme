## TCU Department Theme

Author: Mayra Perales <m.j.perales@tcu.edu>

*******************************************************************

**v2.2.10**

- Added tcu-breadcrumb script to support the events plugin
- Added tribe-events directory that includes style enhancements

**v2.2.9**

- Added a "Read More" link in the news section of flexible content
- Fixed PHP error in content-tcunews.php
- Deleted calendar-feed template part
- Deleted two column section in flexible content
- Removed ability to access video through tabs in the video background section
- Added aria-labels to RSS feed
- Added focus styles to infographics section
- Added ability to upload image background to expandable banner section
- Added height and width to SVG icons
- Now we only load our main JS file in the flexible-content template

**v2.1.9**

- Added a tabs page template
- Deleted overview and areas of study page template
- Cleaned up spaces and code

**v2.0.9**

- Added margin to pagination
- Added bottom padding in overview section
- removed unwanted CSS classes
- Added word-wrap to headings

**v2.0.8**

- Fixed read more link in calendar section
- Removed list bullets from RSS feed
- Removed extra dot character within RSS feed in flexible content
- Cleaned ACF fields
- Added padding to tag cloud
- Fixed PHP code to meet PSR standards

**v2.0.7**

- Added RSS feed section to the flexible content page

**v2.0.6**

- Changed the Overview Section background color to $text-color
- Changed the text color in the expandable banner section
- Added a fallback background color for the highlight cards
- Changed headings for flexible content sections
- Added a main div for the skip-nav link
- Fixed empty button in expandable banner
- Added image input field for expandable banner
- Fixed expandable banner styling
- Fixed img resizing in feature section
- Removed background image in overview section

**v2.0.5**

- Replaced ACF json files with PHP includes
- Fixed the News section read more button

**v2.0.4**

- Added Active Data Calendar feed
- cleaned up code
- Added Arvo Font to our variables
- fixed video not resizing in feature section

**v2.0.3**

- updated flexbox styles
- added news flexible content
- removed front-page.php
- changed the accordion h3 to a button tag for accessibility

**v2.0.2**

- moved js to footer
- moved admin-ajax.php to footer
- concat all js files to make only one http/s call
- optimized images

**v2.0.1**

- center images in sidebar
- added a flexible content page template
- added padding to tcu-expand-banner-child

**v2.0.0**

- complete redesign of front-page
- autoload acf json

**v1.0.1**

- Added widget homepage widget areas
- Added page-overview page template
- Added quote box for homepage

**v1.0.0**

- Initial theme
