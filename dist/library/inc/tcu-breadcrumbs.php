<?php
/*
 * Let's override the parent's breadcrumb scipt
 * We want to be able to include 'events'
 */
function tcu_breadcrumbs_list()
{

    /* === OPTIONS === */
    $text['cd_name']  = get_option('tcu_breadcrumbs_name');
    $text['cd_url']   = get_option('tcu_breadcrumbs_url');
    $text['home']     = get_bloginfo('name'); // text for the 'Home' link
    $text['category'] = __('Archive for %s', 'tcu_web_standards'); // text for a category page
    $text['search']   = __('Search results for: %s', 'tcu_web_standards'); // text for a search results page
    $text['tag']      = __('Posts tagged %s', 'tcu_web_standards'); // text for a tag page
    $text['author']   = __('View all posts by %s', 'tcu_web_standards'); // text for an author page
    $text['404']      = __('Error 404', 'tcu_web_standards'); // text for the 404 page

    $show['current'] = 1; // 1 - show current post/page title in breadcrumbs, 0 - don't show
    $show['home']    = 1; // 1 - show breadcrumbs on the homepage, 0 - don't show

    $delimiter = '<span class="tcu-breadcrumbs__chevron">&#8250;</span> '; // delimiter between crumbs
    $before    = '<span class="tcu-breadcrumbs__current">'; // tag before the current crumb
    $after     = '</span>'; // tag after the current crumb
    /* === END OF OPTIONS === */

    /* === THE TCU.EDU LINK === */
    $tcu_link    = '<span class="tcu-breadcrumbs__crumb" typeof="v:Breadcrumb"><a class="tcu-icon-home" href="http://www.tcu.edu" title="Texas Christian University"><svg height="18" width="20"><use xlink:href="#home-icon"></use></svg><span aria-hidden="true" class="tcu-visuallyhidden">Texas Christian University</span></a></span>';
    $cd_link     = '<span class="tcu-breadcrumbs__crumb" typeof="v:Breadcrumb"><a rel="v:url" property="v:title" href="'.$text['cd_url'].'">'.$text['cd_name'].'</a></span>';
    $home_link   = home_url('/');
    $before_link = '<span class="tcu-breadcrumbs__crumb" typeof="v:Breadcrumb">';
    $after_link  = '</span>';
    $link_att    = ' rel="v:url" property="v:title"';
    $link        = $before_link . '<a' . $link_att . ' href="%1$s">%2$s</a>' . $after_link;

    $post      = get_queried_object();
    $parent_id = isset($post->post_parent) ? $post->post_parent : '';

    $html_output = '';

    // Front page
    if (is_front_page()) {

        // show current post/page title in breadcrumbs
        if (1 == $show['home']) {
            if (!empty($text['cd_name']) && !empty($text['cd_url'])) {
                $html_output .= '<nav class="tcu-breadcrumbs"><div class="tcu-layout-constrain cf">'.$tcu_link.$delimiter.$cd_link.$delimiter.'<a href="' . $home_link . '">' . $text['home'] . '</a></div></nav>';
            } else {
                $html_output .= '<nav class="tcu-breadcrumbs"><div class="tcu-layout-constrain cf">'.$tcu_link. $delimiter.'<a href="' . $home_link . '">' . $text['home'] . '</a></div></nav>';
            }
        }
        // If we are showing a unit/department
    } else {
        // If the custom Breadcrumbs settings are empty
        if (!empty($text['cd_name']) && !empty($text['cd_url'])) {
            $html_output .= '<nav class="tcu-breadcrumbs" xmlns:v="http://rdf.data-vocabulary.org/#"><div class="tcu-layout-constrain cf">' .$tcu_link. $delimiter.$cd_link.$delimiter. sprintf($link, $home_link, $text['home']) . $delimiter;
        } else {
            $html_output .= '<nav class="tcu-breadcrumbs" xmlns:v="http://rdf.data-vocabulary.org/#"><div class="tcu-layout-constrain cf">' .$tcu_link. $delimiter. sprintf($link, $home_link, $text['home']) . $delimiter;
        }

        if (is_home()) {
            if (1 == $show['current']) {
                $html_output .= $before . get_the_title(get_option('page_for_posts', true)) . $after;
            }
            // For the events plugin
        } elseif (is_category()) {
            $this_cat = get_category(get_query_var('cat'), false);
            if (0 != $this_cat->parent) {
                $cats = get_category_parents($this_cat->parent, true, $delimiter);
                $cats = str_replace('<a', $before_link . '<a' . $link_att, $cats);
                $cats = str_replace('</a>', '</a>' . $after_link, $cats);
                $html_output .= $cats;
            }

            $html_output .= $before . sprintf($text['category'], single_cat_title('', false)) . $after;
        } elseif (is_tax()) {

            $post_type    = get_post_type_object(get_post_type());
            $archive_link = get_post_type_archive_link($post_type->name);
            $terms        = get_terms();
            $this_tax     = get_query_var('term');
            $current_tax  = array_filter($terms, function ($x) {
                return $x->slug == get_query_var('term');
            });

            // $html_output .= sprintf($link, $archive_link, $post_type->labels->singular_name);
            $key = key($current_tax);
            if (0 == $current_tax[$key]->parent && 'page'!== $post_type->name) {
                $post_type_link = sprintf($link, $archive_link, $post_type->labels->singular_name);
                $html_output .= $delimiter . $before . $current_tax[$key]->name . $after;
            } elseif('page' === $post_type->name) {
                // For the Events plugin - category
                $events_link = tribe_get_events_link();
                $events_title = tribe_get_events_title();
                $html_output .= sprintf($link, $events_link, $events_title);
            } else {
                $parent      = get_term_by('term_id', (int) $current_tax[$key]->parent, $current_tax[$key]->taxonomy);
                $parent_url  = get_term_link($parent);
                $parent_link = $delimiter . sprintf($link, $parent_url, $parent->name);
                $html_output .= $parent_link . $delimiter . $current_tax[$key]->name;
            }

        } elseif ((function_exists('tribe_is_list_view') || function_exists('tribe_is_month')) && is_tax()) {
            if ((tribe_is_list_view() || tribe_is_month()) && is_tax()) {
                global $wp_query;
                $term_slug = $wp_query->query_vars['tribe_events_cat'];
                $term = get_term_by('slug', $term_slug, 'tribe_events_cat');
                $name = $term->name;
                $upcoming_events = tribe_get_events_link();
                $html_output .= $before_link.sprintf($link, $upcoming_events, tribe_get_events_title()).$after_link;
            }
        } elseif (is_search()) {
            $html_output .= $before . sprintf($text['search'], get_search_query()) . $after;
        } elseif (is_day()) {
            $html_output .= sprintf($link, get_year_link(get_the_time('Y')), get_the_time('Y')) . $delimiter;
            $html_output .= sprintf($link, get_month_link(get_the_time('Y'), get_the_time('m')), get_the_time('F')) . $delimiter;
            $html_output .= $before . get_the_time('d') . $after;
        } elseif (is_month()) {
            $html_output .= sprintf($link, get_year_link(get_the_time('Y')), get_the_time('Y')) . $delimiter;
            $html_output .= $before . get_the_time('F') . $after;
        } elseif (is_year()) {
            $html_output .= $before . get_the_time('Y') . $after;
        } elseif (is_single() && !is_attachment()) {
            if ('post' != get_post_type()) {
                $post_type    = get_post_type_object(get_post_type());
                $archive_link = get_post_type_archive_link($post_type->name);

                if ($archive_link) {
                    $html_output .= sprintf($link, $archive_link, $post_type->labels->singular_name).$delimiter;
                }

                // For the events plugin
                if ('page' === $post_type->name) {
                    global $wp_query;
                    $upcoming_events = tribe_get_events_link();

                    if (array_key_exists('tribe_events', $wp_query->query)) {
                        $term_slug = $wp_query->query['tribe_events'];
                        $single_name = ucwords(str_replace('-', ' ', $term_slug));
                        $html_output .= $before_link.sprintf($link, $upcoming_events, tribe_get_events_title()).$after_link.$delimiter.$single_name;
                    }

                    if (array_key_exists('tribe_venue', $wp_query->query)) {
                        $venue_slug = $wp_query->query['tribe_venue'];
                        $single_venue = ucwords(str_replace('-', ' ', $venue_slug));
                        $html_output .= $before_link.sprintf($link, $upcoming_events, tribe_get_events_title()).$after_link.$delimiter.$single_venue;
                    }
                }

                if (1 == $show['current']) {
                    $html_output .= $before . get_the_title() . $after;
                }
            } else {
                $cat  = get_the_category();
                $cat  = $cat[0];
                $cats = get_category_parents($cat, true, $delimiter);

                if (0 == $show['current']) {
                    $cats = preg_replace("#^(.+)$delimiter$#", "$1", $cats);
                }

                $cats = str_replace('<a', $before_link . '<a' . $link_att, $cats);
                $cats = str_replace('</a>', '</a>' . $after_link, $cats);
                $html_output .= $cats;

                if (1 == $show['current']) {
                    $html_output .= $before . get_the_title() . $after;
                }
            }
        } elseif (!is_single() && !is_page() && !is_404() && 'post' != get_post_type()) {
            $post_type = get_post_type_object(get_post_type());

            if ('page' === $post_type->name) {
                $html_output .= $before.tribe_get_events_title().$after;
            }

            if ($post_type && 'page' != $post_type->name) {
                $html_output .= $before . $post_type->labels->singular_name . $after;
            }
        } elseif (is_attachment()) {
            $parent = get_post($parent_id);
            $cat    = get_the_category($parent->ID);

            if (isset($cat[0])) {
                $cat = $cat[0];
            }

            if ($cat) {
                $cats = get_category_parents($cat, true, $delimiter);
                $cats = str_replace('<a', $before_link . '<a' . $link_att, $cats);
                $cats = str_replace('</a>', '</a>' . $after_link, $cats);
                $html_output .= $cats;
            }

            $html_output .= sprintf($link, get_permalink($parent), $parent->post_title);
            if (1 == $show['current']) {
                $html_output .= $delimiter . $before . get_the_title() . $after;
            }
        } elseif (is_page() && !$parent_id) {
            if (1 == $show['current']) {
                $html_output .= $before . get_the_title() . $after;
            }
        } elseif (is_page() && $parent_id) {
            $breadcrumbs = array();
            while ($parent_id) {
                $page_child    = get_post($parent_id);
                $breadcrumbs[] = sprintf($link, get_permalink($page_child->ID), get_the_title($page_child->ID));
                $parent_id     = $page_child->post_parent;
            }

            $breadcrumbs = array_reverse($breadcrumbs);

            for ($i = 0; $i < count($breadcrumbs); $i++) {
                $html_output .= $breadcrumbs[$i];

                if ($i != count($breadcrumbs) - 1) {
                    $html_output .= $delimiter;
                }
            }
            if (1 == $show['current']) {
                $html_output .= $delimiter . $before . get_the_title() . $after;
            }
        } elseif (is_tag()) {
            $html_output .= $before . sprintf($text['tag'], single_tag_title('', false)) . $after;
        } elseif (is_author()) {
            $user_id  = get_query_var('author');
            $userdata = get_the_author_meta('display_name', $user_id);
            $html_output .= $before . sprintf($text['author'], $userdata) . $after;
        } elseif (is_404()) {
            $html_output .= $before . $text['404'] . $after;
        }

        if (get_query_var('paged') || get_query_var('page')) {
            $page_num = get_query_var('page') ? get_query_var('page') : get_query_var('paged');
            $html_output .= $delimiter . sprintf(__('Page %s', 'tcu_web_standards'), $page_num);
        }

        $html_output .= '</div></nav>';
    }

    echo $html_output;

} // end responsive_breadcrumb_lists
