<?php
/**
 * Enqueue parent style sheet
 * as well as the child style sheet
 */
function my_child_theme_enqueue_styles()
{
    $child_style  = 'child-style';

    wp_enqueue_style($child_style,
        /* Edit the path to your child themes stylesheet */
        get_stylesheet_directory_uri() . '/library/css/style.min.css',
        array( 'tcu-stylesheet' ), '2.2.10'
    );

    // Make sure we only load our JS in the flexible content template
    if(is_page_template('page-flexible-content.php')) {
        //adding scripts file in the footer - this file includes ALL our JS files
        wp_register_script('tcu-department-scripts', get_stylesheet_directory_uri() . '/library/js/min/tcu-academic-scripts.min.js', array( 'jquery' ), '2.2.10', true);

        wp_enqueue_script('tcu-department-scripts');
    }

}

add_action('wp_enqueue_scripts', 'my_child_theme_enqueue_styles');

/*************************** INCLUDES ********************************/

require_once('wp-advanced-search/wpas.php');

foreach (glob(plugin_dir_path(__FILE__) . '/library/inc/*.php') as $file) {
    require_once($file);
}

/************* IMAGE SIZES *************/

// Thumbnail sizes
add_image_size('tcu-480-550', 480, 550, true);
add_image_size('tcu-700-550', 700, 550, true);
add_image_size('tcu-1000-550', 1000, 550, true);
add_image_size('tcu-1800-550', 1800, 550, true);

/***************************** ACF Plugin *****************************/

/**
 * Check if the ACF plugin is intalled
 *
 * @return bool  True if the ACF plugin is installed
 */
function tcu_academic_child_theme_acf() {
    /* Checks to see if "is_plugin_active" function exists and if not load the php file that includes that function */
    if ( !function_exists( 'is_plugin_active' ) ) {
        include_once( ABSPATH . 'wp-admin/includes/plugin.php' );
    }

    /* Checks to see if the acf pro plugin is activated  */
    if ( is_plugin_active('advanced-custom-fields-pro/acf.php') || is_plugin_active('advanced-custom-fields/acf.php') )  {
        return true;
    } else {
        return false;
    }
}

/*
 * Admin notice to display when ACF is not installed
 */
function tcu_academic_child_theme_notice_missing_acf()
{
    global $tp_acf_notice_msg;

    $tp_acf_notice_msg = __( 'This website needs "Advanced Custom Fields PRO" plugin to run. Please download and activate it.', 'tcu_academic_child_theme' );

    if(! tcu_academic_child_theme_acf() ) {
        echo '<div class="notice notice-error notice-large"><div class="notice-title">'. $tp_acf_notice_msg .'</div></div>';
    }
}

add_action( 'admin_notices', 'tcu_academic_child_theme_notice_missing_acf' );

/**************************
 * Advanced Search
 *************************/
function tcu_addran_fs_search()
{
    $args = array();

    $args['wp_query'] = array(
                        'post_type' => array('faculty_staff'),
                        'orderby'   => 'meta_key',
                        'meta_key'  => 'tcu_last_name',
                        'order'     => 'ASC'
                      );
    // The AJAX
    $args['form'] = array( 'auto_submit' => true );

    $args['form']['ajax'] = array(
                            'enabled'              => true,
                            'show_default_results' => false,
                            // This file must exist in your theme root
                            'results_template'     => 'template-fs-results.php'
                          );
    // Configure form fields
    $args['fields'][] = array(
                        'type'           => 'meta_key',
                        'label'          => 'Last Name *',
                        'meta_key'       => 'tcu_last_name',
                        'format'         => 'text',
                        'post_type_slug' => 'faculty_staff',
                        'compare'        => 'LIKE',
                        'data_type'      => 'ARRAY<CHAR>'
                      );

    $args['fields'][] = array(
                        'type'           => 'meta_key',
                        'label'          => 'First Name',
                        'meta_key'       => 'tcu_first_name',
                        'format'         => 'text',
                        'post_type_slug' => 'faculty_staff',
                        'compare'        => 'LIKE',
                        'data_type'      => 'ARRAY<CHAR>'
                      );

    $args['fields'][] = array(
                        'type'  => 'submit',
                        'class' => array('tcu-button', 'tcu-button--primary', 'tcu-bounce', 'tcu-bounce--right--grey', 'tcu-top12'),
                        'value' => 'Submit'
                      );

    register_wpas_form('tcu_addran_fs_searchform', $args);
}

add_action('init', 'tcu_addran_fs_search');

/**
 * Hide ACF except for Super Admins and Admins
 *
 */
add_filter('acf/settings/show_admin', 'tcu_acf_show_admin');

function tcu_acf_show_admin($show)
{
    return current_user_can('manage_options');
}

/**
 * TCU Slider Custom Post Type
 * Select Sliders in our flexible content page template
 *
 * @param  array  $field  The field settings array.
 * @return array  $field  Modified settings array.
 */
function tcu_acf_load_slider_field_choices($field)
{

    // reset choices
    $field['choices'] = array();

    // bail if TCU Slider plugin is not installed
    if (class_exists('TCU_Display_Slider')) {

        $choices = TCU_Display_Slider::all_sliders();

        $new_choices = array();

        foreach ($choices as $choice) {
            array_push($new_choices, $choice['post_title'] . " (" . $choice['ID'] . ")");
        }

        if (is_array($new_choices)) {
            foreach ($new_choices as $choice) {
                $field['choices'][ $choice ] = $choice;
            }
        } else {
            $field['choices'][ $new_choices ] = $new_choices;
        }
    }
    // return the field
    return $field;
}

add_filter('acf/load_field/name=select_slider', 'tcu_acf_load_slider_field_choices');


/**
 * TCU News Custom Post Type
 * Select keywords in our flexible content page template
 *
 * @param  array  $field  The field settings array.
 * @return array  $field  Modified settings array.
 */
function tcu_acf_load_news_keyword_choices($field)
{

    // bail if TCU News is not installed
    if (!function_exists('tcu_news_query')) {
        return;
    }

    // reset choices
    $field['choices'] = array();

    // get all keywords
    $keywords = get_terms(array( 'taxonomy'   => 'tcu_news_keyword', 'hide_empty' => false ));

    // Make sure we have the default selection
    $new_choices = array('View all news ( tcu_all_news )');

    foreach ($keywords as $key) {
        array_push($new_choices, $key->name . " (" . $key->slug . ")");
    }

    if (is_array($new_choices)) {
        foreach ($new_choices as $key) {
            $field['choices'][ $key ] = $key;
        }
    } else {
        $field['choices'][ $new_choices ] = $new_choices;
    }

    // return the field
    return $field;
}

add_filter('acf/load_field/name=tcu_news_select_keyword', 'tcu_acf_load_news_keyword_choices');

/********************* TCU RSS NEWS  ******************************/

/**
 * Process RSS feed and
 * create a transient to cache results
 *
 * @param $rss string  URL for the RSS feed
 * @param $num int     The number of stories to fetch
 *
 * @return $story      HTML shell with stories
 */
function tcu_process_rss_feed($rss, $num = 5)
{

    // Get a SimplePie feed object from the specified feed source.
    $rss = fetch_feed($rss);

    $maxitems = 0;

    // Checks that the object is created correctly
    if (! is_wp_error($rss)) {

        // Figure out how many total items there are, but limit it
        $maxitems = $rss->get_item_quantity($num);

        // Build an array of all the items, starting with element 0
        $rss_items = $rss->get_items(0, $maxitems);
    }

    $story = '<div class="tcu-masonry-home effect-2 cf" id="tcu-masonry-home">';

        // If RSS feed is empty
        if ($maxitems == 0) {
            $story .= _e('<p>No news items at the moment.</p>', 'tcu-department-child-theme');
        } else {

            // Loop through each feed item
            foreach ($rss_items as $item) {

                // Grab the entire content
                $content = $item->get_content();

                // Let's grab the IMG tag from inside the content
                if (preg_match('/(<img[^>]+>)/i', $content, $matches)) {
                    $img = $matches[0];
                } else {
                    $img = '';
                }


                $story .= '<article class="tcu-article tcu-modal cf" role="article">';
                $story .= $img;
                $story .= '<section style="clear: both;" class="tcu-modal__content cf">';
                $story .= '<h5 class="tcu-arvo tcu-mar-b0 h4">'.$item->get_title().'</h5>';
                $story .= '<p class="tcu-byline">'.$item->get_date('j F Y').'</p>';

                $story .= substr(strip_tags($item->get_content()), 0, 150) . '...';
                $story .= '</section>';
                $story .= '<a aria-label="'. __('Read more about ', 'tcu-academic-child-theme') .sanitize_text_field($item->get_title()).'" title="'.esc_attr($item->get_title()).'" class="tcu-button tcu-button--primary tcu-bounce tcu-bounce--right--grey tcu-full-width" href="'.esc_url($item->get_permalink()).'">Read More<svg height"30" width="30"><use xlink:href="#play-icon"></use></svg></a>';
                $story .= '</article><!-- end of .tcu-article -->';
            }
        }

    $story .= '</div><!-- end of .tcu-masonry -->';

    return $story;

} // don't remove this bracket!
