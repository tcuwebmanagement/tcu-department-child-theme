# TCU Department Child Theme
This theme should only be used with the TCU Web Standards parent theme.

__Web Standards Theme for TCU__

The TCU Web Standards theme is designed to incorporate TCU Brand Standards. It is built using HTML5 and has a strong foundation.

__GETTING STARTED__

We use GRUNT as our task runner. All our WordPress production files are located in dist/ directory. Check out our Gruntfile.js for instructions on how the set up is used. We use BEM for naming all our classes. We also use a prefix of "tcu" within our CSS classes.

__Our Files__

We offer two versions — a minified version, and an un-minified one. Use the minified version in a production environment or to reduce the file size of your downloaded assets. And the un-minified version is better if you are in a development environment or would like to debug the CSS or JavaScript assets in the browser.


__Our main SASS files are__

    library/scss

__WordPress Theme__

Run `grunt zip` to compress a clean zip version of the dist/ directory. From the WordPress admin screen, select Appearance > Themes from the menu. Click on Add new and select the zip file you just created to install. Finally, activate the theme.

    dist/

__jQuery__

WordPress comes with jQuery installed. Make sure to include jQuery if you are not using a WordPress theme

__Working with an existing Grunt project__

Assuming that the Grunt CLI has been installed and that the project has already been configured with a package.json and a Gruntfile, it's very easy to start working with Grunt:

    - Change to the project's root directory.
    - Install project dependencies with npm install.
    - Run Grunt with grunt.

That's really all there is to it. Installed Grunt tasks can be listed by running grunt --help but it's usually a good idea to start with the project's documentation.

__Install NPM__

__[Download NPM](https://www.npmjs.com/)__

__GRUNT PLUGINS__

If you haven't used Grunt before, be sure to check out the __[Getting Started Guide](http://gruntjs.com/getting-started)__, as it explains how to create a __[Gruntfile](http://gruntjs.com/sample-gruntfile)__ as well as install and use Grunt plugins.

Gruntfile.js is ready to use the following plugins:

    - npm install grunt-contrib-concat --save-dev
    - npm install grunt-contrib-sass --save-dev
    - npm install autoprefixer --save-dev
    - npm install grunt-contrib-cssmin --save-dev
    - npm install grunt-contrib-uglify --save-dev
    - npm install grunt-contrib-watch --save-dev
    - npm install grunt-contrib-imagemin --save-dev
    - npm install grunt-contrib-copy --save-dev
    - npm install grunt-newer --save-dev
    - npm install grunt-contrib-compress --save-dev


Developed by **Mayra Perales**: <m.j.perales@tcu.edu>

## Special thanks to:

    - Eddie Machado - http://themble.com/bones
    - Paul Irish & the HTML5 Boilerplate
    - Yoast for some WP functions & optimization ideas
    - Andrew Rogers for code optimization
    - David Dellanave for speed & code optimization
    - and several other developers. :)

## Submit Bugs & or Fixes:
https://TCUWebmanage@bitbucket.org/TCUWebmanage/tcu_web_standards.git


## Meta
* [Changelog](CHANGELOG.md)
