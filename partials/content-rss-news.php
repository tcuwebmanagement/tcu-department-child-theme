<?php
/*
 * Template part to display RSS News feed
 */

$title     = get_sub_field('rss_tcu_news_feed_title');
$rss       = get_sub_field('rss_tcu_news_feed_url');
$maxnum    = get_sub_field('rss_tcu_news_feed_items');
$more_link = get_sub_field('rss_tcu_news_feed_read_more');
?>

<div class="tcu-layoutwrap--transparent cf">

    <div class="tcu-layout-constrain cf">

        <?php if ($title) : ?>
            <h4 class="tcu-mar-t0 tcu-mar-t0 tcu-arvo tcu-alignc h2"> <?php echo $title; ?></h4>
        <?php endif; ?>

        <?php echo tcu_process_rss_feed($rss, $maxnum); ?>

        <div class="tcu-layout-center tcu-alignc tcu-top32 tcu-below32 cf">
            <!-- Our Read More button -->
            <a title="Read More News" class="tcu-button tcu-button--primary tcu-bounce tcu-bounce--right--grey tcu-alignc" href="<?php echo esc_url($more_link); ?>">More News</a>
        </div>

    </div><!-- end of .tcu-layout-constrain -->

</div><!-- end of .tcu-layoutwrap--transparent -->