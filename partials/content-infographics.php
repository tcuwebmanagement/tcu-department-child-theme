<?php
/*
 * Template part to display the inforgraphics
 */
?>
<div class="tcu-layoutwrap--transparent tcu-pad-lr0 cf">

  <div class="tcu-layout-constrain tcu-infographics cf">

    <?php
      // check if the flexible content field has rows of data
      if (have_rows('icons_infographics_section')):

        /*
         * Start the ACF loop.
         */
        while (have_rows('icons_infographics_section')) : the_row();

            $image  = get_sub_field('icon_infographics_section');
            $link   = get_sub_field('link_infographics_section');
            $title  = get_sub_field('title_infographics_section');

            $html  = '<div class="tcu-graphics"><a class="cf" href="' . esc_url($link) . '">';
            $html .= '<img src="' . esc_url($image['url']) . '" alt="' . esc_attr($title) . '" />';
            if ($title) :
                $html .= '<h3 class="h3">' . sanitize_text_field($title) . '</h3>';
            endif;
            $html .= '</a></div>';

            echo $html;
            /*
             * End the ACF loop.
             */
            endwhile;

        endif;
    ?>
  </div><!-- end of .tcu-infographics -->

</div><!-- end of .tcu-layoutwrap--transparent -->
