<?php
/**
 * Template part to display video section
 */

// Variables
$text  = get_sub_field('content_video_section');
$video = get_sub_field('oembed_video_section');

// use preg_match to find iframe src
if (preg_match('/src="(.+?)"/', $video, $matches)) {
    $src = $matches[1];
} else {
    $src = '';
}

// Find the video ID
if($src) {
    preg_match('%(?:youtube(?:-nocookie)?\.com/(?:[^/]+/.+/|(?:v|e(?:mbed)?)/|.*[?&]v=)|youtu\.be/)([^"&?/ ]{11})%i', $src, $match);
}


// add extra params to iframe src
$params = array(
    'controls'    => 0,
    'showinfo'    => 0,
    'autoplay'    => 1,
    'loop'        => 1,
    'rel'         => 0,
    'hd'          => 1,
    'enablejsapi' => 1,
    'autohide'    => 1,
    'playlist'    => $match[1]
);

// rebuild our URL
$new_src = add_query_arg($params, $src);

// add extra params to iframe src
$video = str_replace($src, $new_src, $video);

// add extra attributes to iframe html
$attributes = 'frameborder="0" tabindex="-1" id="ytplayer"';

$video = str_replace('></iframe>', ' ' . $attributes . '></iframe>', $video);

// Mute our video with the script below
?>
<script>
    var tag = document.createElement('script');
    tag.src = "//www.youtube.com/iframe_api";
    var firstScriptTag = document.getElementsByTagName('script')[0];
    firstScriptTag.parentNode.insertBefore(tag, firstScriptTag);

    function onYouTubePlayerAPIReady() {

    player = new YT.Player('ytplayer', {
        events: {
            'onReady': function(e) {
                e.target.mute();
            }
        }
    });
  }
</script>

<div class="tcu-layoutwrap--purple tcu-overflow-hidden tcu-pad-tb0 tcu-pad-lr0">
    <div class="tcu-video">
        <div class="tcu-video-container">
            <?php echo $video; ?>
        </div>
        <?php if (! empty($text)) : ?>
        <div class="tcu-hero__content tcu-flexbox tcu-flexbox--column tcu-flexbox--vertical-align">
            <h2 class="tcu-line-height-1 tcu-arvo"><?php echo sanitize_text_field($text); ?></h2>
        </div>
        <?php endif; ?>
        </div>
    </div>
