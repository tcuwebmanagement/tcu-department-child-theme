<?php
/*
 * Template part to display Highlight box section
 */


if (have_rows('highlight_section_repeat')) : ?>

<div class="tcu-layoutwrap--grey tcu-pad-tb0 tcu-pad-lr0 cf">

    <div class="tcu-layout--large tcu-layout-center">

    <?php
    /*
     * Start the ACF loop.
     */
    while (have_rows('highlight_section_repeat')) : the_row();

    // ACF Variables
    $image     = get_sub_field('image_highlight_section');
    $title     = get_sub_field('title_highlight_section');
    $content   = get_sub_field('content_highlight_section');
    $link      = get_sub_field('link_highlight_section');
    $text      = get_sub_field('link_text_highlight_section_repeat');
    $arialabel = get_sub_field('aria_label_highlight_section_repeat');

/**
 * We inlined our styles in order to change the image size for each media query
 * Faster loading time and smaller images for mobile
 * Performance is important!
 */
?>
<?php if (! empty($image)) : ?>
<style media="screen">
#tcu-highlight-<?php echo $image['name'] ?> {
    background: linear-gradient(rgba(77, 25, 121, 0.6), rgba(77, 25, 121, 0.6)), #4D1979 url('<?php echo $image["sizes"]["tcu-480-550"] ?>') center center no-repeat;
}

#tcu-highlight-<?php echo $image['name'] ?>:nth-child(3n+2) {
  background: linear-gradient(rgba(0, 0, 0, 0.45), rgba(0, 0, 0, 0.45)), #111111 url('<?php echo $image["sizes"]["tcu-480-550"] ?>') center center no-repeat;;
}

@media screen and ( min-width: 481px ) {
    #tcu-highlight-<?php echo $image['name'] ?> {
        background: linear-gradient(rgba(77, 25, 121, 0.6), rgba(77, 25, 121, 0.6)), #4D1979 url('<?php echo esc_url($image["sizes"]["tcu-1000-550"]); ?>') center center no-repeat;

    }

    #tcu-highlight-<?php echo $image['name'] ?>:nth-child(3n+2) {
      background: linear-gradient(rgba(0, 0, 0, 0.45), rgba(0, 0, 0, 0.45)), #111111 url('<?php echo $image["sizes"]["tcu-1000-550"] ?>') center center no-repeat;;
    }
}

@media screen and ( min-width: 1000px ) {
    #tcu-highlight-<?php echo $image['name'] ?> {
        background: linear-gradient(rgba(77, 25, 121, 0.6), rgba(77, 25, 121, 0.6)), #4D1979 url('<?php echo esc_url($image["sizes"]["tcu-1800-550"]); ?>') center center no-repeat;

    }

    #tcu-highlight-<?php echo $image['name'] ?>:nth-child(3n+2) {
      background: linear-gradient(rgba(0, 0, 0, 0.45), rgba(0, 0, 0, 0.45)), #111111 url('<?php echo $image["sizes"]["tcu-1800-550"] ?>') center center no-repeat;;
    }
}
</style>
<?php endif; ?>

        <div id="tcu-highlight-<?php echo $image['name'] ?>" class="unit size1of2 m-size1of1 tcu-background-defaults">

          <div class="tcu-zindex-2">

            <?php if (! empty($title)) : ?>
                <h3 class="tcu-uppercase h2"><?php echo sanitize_text_field($title); ?></h3>
            <?php endif; ?>

            <?php if (! empty($content)) : ?>
                <p><?php echo esc_textarea($content); ?></p>
            <?php endif; ?>

            <?php
                $html = '<a';
                if($arialabel) {
                    // Add a space at the front so it concatenates correctly
                    $html .= ' aria-label="'.esc_attr($arialabel).'"';
                }
                // Add a space at the front so it concatenates correctly
                $html .= ' class="tcu-button tcu-button--transparent tcu-bounce tcu-bounce--right--yellow"';
                $html .= ' href="'.esc_url($link).'">'.sanitize_text_field($text).' <svg height="30" width="30" class="tcu-button-icon"><use xlink:href="#circle-next-arrow"></use></svg></a>';

                // Display our correct hyperlink tag
                echo $html;
            ?>

          </div><!-- end of .tcu-zindex-2 -->

        </div><!-- end of .tcu-background-defaults -->

    <?php
    /*
     * End of the ACF loop.
     */
    endwhile;
    ?>
    </div>

</div><!-- end of .tcu-layoutwrap--grey -->
 <?php endif; ?>
