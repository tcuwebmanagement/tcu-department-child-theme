<?php
/**
 * Template part to display the Overview Section
 */

// ACF Variables
$content   = get_sub_field('content_overview_section');
$link      = get_sub_field('link_overview_section');
$text      = get_sub_field('link_text_overview_section');
$arialabel = get_sub_field('aria-label_overview_section');
?>

<div class="tcu-layoutwrap--darkgrey tcu-pattern--angled-lines">

    <div class="tcu-layout--large tcu-flexbox tcu-flexbox--column tcu-flexbox--vertical-align tcu-overview-block">

      <div>

        <?php if (! empty($content)) : ?>
        <blockquote><?php echo esc_textarea($content); ?></blockquote>
        <?php endif; ?>

        <?php
            $html = '<a';
            if($arialabel) {
                // Add a space at the front so it concatenates correctly
                $html .= ' aria-label="'.esc_attr($arialabel).'"';
            }
            // Add a space at the front so it concatenates correctly
            $html .= ' class="tcu-button tcu-button--transparent tcu-bounce tcu-bounce--right--yellow"';
            $html .= ' href="'.esc_url($link).'">'.sanitize_text_field($text).' <svg height="30" width="30" class="tcu-button-icon"><use xlink:href="#circle-next-arrow"></use></svg></a>';

            // Display our correct hyperlink tag
            echo $html;
        ?>

      </div>

    </div>

</div>
