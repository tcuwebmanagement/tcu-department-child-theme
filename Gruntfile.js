module.exports = function(grunt){

    grunt.initConfig({

        // Let's combine all our JS files into one
        concat: {
            // Front-end
            main: {
                src: [
                    'library/js/classie.js',
                    'library/js/imagesloaded.pkgd.js',
                    'library/js/masonry.pkgd.js',
                    'library/js/AnimOnScroll.js',
                    'library/js/device.js',
                    'library/js/tcu-department-scripts.js'
                ],
                dest: 'library/js/concat/tcu-academic-scripts.js',
            },
        },
        // Let's minimize our JS files
        uglify: {
            // Front-end
            main: {
                src: 'library/js/concat/tcu-academic-scripts.js',
                dest: 'library/js/min/tcu-academic-scripts.min.js'
            }
        },

        //  Let's compile our SASS into CSS
        sass: {
            dist: {
                options: {
                    style: 'expanded'
                },
                files: {
                    'library/css/style.css': 'library/scss/style.scss'
                }
            }
        },

        // Let's provide our users a minified version of all our CSS files
        // This is done when you're ready to provide a final version copy
        cssmin: {
            target: {
                files: [
                    {
                        expand: true,
                        cwd: 'library/css/',
                        src: ['*.css', '!*.min.css'],
                        dest: 'library/css/',
                        ext: '.min.css'
                    }
                ]
            }
        },

        // Autoprefix and other polyfills
        postcss: {
            options: {
                syntax: require('postcss-scss'), // work with SCSS directly
                processors: [require('autoprefixer')({ browsers: '> 5%, last 2 versions, Firefox ESR, Opera 12.1' })]
            },

            dist: {
                src: 'library/scss/**/*.scss'
            }
        },

        // Let's optimize our images
        imagemin: {
            dynamic: {
                options: {
                    optimizationLevel: 7,
                    svgoPlugins: [
                    {
                        removeViewBox: false ,
                        removeAttrs: { attrs: ['xmlns'] }
                    }
                ]
            },
            files: [{
                    expand: true,
                    cwd: 'library/images/',
                    src: ['**/*.{png,jpg,jpeg,gif,svg}'],
                    dest: 'dist/library/images/'
                }]
            }
        },

        // This creates a clean WP theme copy to use in production
        copy: {
            main: {
                files: [
                    {
                        expand: true,
                        src: [
                            './*.php',
                            './*.css',
                            './CHANGELOG.md',
                            './README.md',
                            './*.png',
                            'partials/*.php',
                            'library/css/**.css',
                            'library/css/!*.map',
                            'library/js/**',
                            'library/scss/**/*.scss',
                            'library/inc/**',
                            'wp-advanced-search/**'
                        ],
                        dest: 'dist/'
                    }
                ]
            }
        },

        // Let's zip up our /dist (production wordpress theme)
        // Change version number in style.css
        compress: {
            main: {
                options: {
                    archive: 'tcu-academic-child-theme.2.2.10.zip'
                },
                files: [
                    {
                        expand: true,
                        cwd: 'dist/',
                        src: ['**'],
                        dest: 'tcu-academic-child-theme/'
                    }
                ]

            }
        },

        // Watch our files while we work
        watch: {
            css: {
                files: ['library/scss/**/*.scss'],
                tasks: ['sass']
            },
            cssmin: {
                files: ['library/css/style.css'],
                tasks: ['newer:cssmin'],
                options: {
                    spawn: false
                }
            },
            scripts: {
                files: [
                    'library/js/classie.js',
                    'library/js/imagesloaded.pkgd.js',
                    'library/js/masonry.pkgd.js',
                    'library/js/AnimOnScroll.js',
                    'library/js/device.js',
                    'library/js/tcu-department-scripts.js'
                ],
                tasks: ['newer:concat']
            },
            uglify: {
                files: ['library/js/concat/tcu-academic-scripts.js'],
                tasks: ['newer:uglify'],
                options: {
                    spawn: false
                }
            }
        }

    });

    grunt.loadNpmTasks('grunt-contrib-concat');
    grunt.loadNpmTasks('grunt-contrib-sass');
    grunt.loadNpmTasks('grunt-postcss')
    grunt.loadNpmTasks('grunt-contrib-cssmin');
    grunt.loadNpmTasks('grunt-contrib-uglify');
    grunt.loadNpmTasks('grunt-contrib-imagemin');
    grunt.loadNpmTasks('grunt-contrib-copy');
    grunt.loadNpmTasks('grunt-contrib-compress');
    grunt.loadNpmTasks('grunt-newer');
    grunt.loadNpmTasks('grunt-contrib-watch');

    // optimize images
    grunt.registerTask('default', ['sass', 'cssmin', 'imagemin', 'concat', 'uglify', 'watch']);

    // Build copy to production directory
    grunt.registerTask('build', ['copy']);

    // Zip our clean directory to easily install in WP
    grunt.registerTask('zip', ['compress']);

};
