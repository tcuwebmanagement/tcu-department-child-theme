// as the page loads, call these scripts
jQuery(document).ready(function($) {

    // hide departments
    $('.tcu-expandablebanner-background').css('display', 'none');
    $('.tcu-arrow-down').click(function(){
        $(this).toggleClass('tcu-arrow-up').toggleClass('tcu-arrow-down');
        $('.tcu-expandablebanner-background').slideToggle();
    });

    /*  IF YOU WANT TO USE DEVICE.JS TO DETECT THE VIEWPORT AND MANIPULATE THE OUTPUT  */

    //Device.js will check if it is Tablet or Mobile - http://matthewhudson.me/projects/device.js/
    if ( !device.tablet() && !device.mobile() ) {
        $("#ytplayer").css( 'display', 'block' );
    } else {
        //jQuery will add the default background to the preferred class
        $('.tcu-video-container').css('display', 'none');
        $("#ytplayer").css( 'display', 'none' );
    }

}); /* end of as page load scripts */

( function( window ) {

    var tcuMasonryHome = document.getElementById( 'tcu-masonry-home' );

    if ( tcuMasonryHome == null ){ return; }

    homeMasonryAnim = new AnimOnScroll( tcuMasonryHome, {
        minDuration : 0.4,
        maxDuration : 0.7,
        viewportFactor : 0.2
    });

    homeMasonryAnim._init();

})();
